package com.springboot.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.springboot.app.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	Employee findByfirstName(String firstName);
	//Optional<Employee> findByEmail(String email);
	
	//find is introducer and ByName is criteria
	//select * from employee where name ="firstName"
	
  
   @Query(value ="SELECT * FROM employees e",nativeQuery = true)
   List<Employee> getAllEmployeesUsingJPAQL();
	
//	@Query("select e from employees e where e.name=:n")
//	public List<Employee> getEmployeeByNameUsingJPQL(@Param("n") String firstName);

}
