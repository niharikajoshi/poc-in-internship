package com.springboot.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableWebMvc
@EnableSwagger2
@SpringBootApplication
@OpenAPIDefinition(info =
@Info
(title = "Employee API", version = "1.0", description = "Documentation Employee API v1.0")
)
public class EcommerceShoppingCartApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceShoppingCartApplication.class, args);
	}
//	 @Bean
//	    public Docket api() { 
//	        return new Docket(DocumentationType.SWAGGER_2)  
//	          .select()                                  
//	          .apis(RequestHandlerSelectors.any())              
//	          .paths(PathSelectors.any())                          
//	          .build(); 
//	          .apiInfo(apiDetails());
//	    }
//	
//	private ApiInfo apiDetails() {
//		 return new ApiInfo(
//				 "Employee Management System",
//				 "Sample API for employee",
//				 "1.0",
//				 "Free to Use",
//				 new springfox.documentation.service.Contact("niharika", "https://javabrains.io/", "nj@gmail.com"),
//				 "API license",
//				 "https://javabrains.io/",
//				 Collections.emptyList());
//	 }
	
}


