package com.springboot.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.springboot.app.exception.ResourceNotFoundException;
import com.springboot.app.model.Employee;
import com.springboot.app.repository.EmployeeRepository;
import com.springboot.app.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	Logger logger = LoggerFactory.getLogger(EmployeeService.class);
	private EmployeeRepository employeeRepository;
	
	public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
		super();
		this.employeeRepository = employeeRepository;
		
	}

	@Override
	public Employee saveEmployee(Employee employee) {
		logger.info("Successfully saved employee");
		return employeeRepository.save(employee);
		
	}

	@Override
	public List<Employee> getAllEmployees() {
		logger.info("returning all employee");
		return employeeRepository.findAll();
	}
	
	

	@Override
	public Employee getEmployeeById(long id) {
//		Optional<Employee> employee = employeeRepository.findById(id);
//		if(employee.isPresent()) {
//			return employee.get();
//		}else {
//			throw new ResourceNotFoundException("Employee", "Id", id);
//		}
		
		//lambda expression 
		return employeeRepository.findById(id).orElseThrow(() -> 
						new ResourceNotFoundException("Employee", "Id", id));
		
	}
	
	@Override 
	public Employee getEmployeeByName(String firstName)
	{
		Employee employee = new Employee();
		try {
			employee = employeeRepository.findByfirstName(firstName);
		} catch (ResourceNotFoundException e) {
			
			e.getMessage();
		}
		return employee;
		
	}
	
	
	public List<Employee> getAllEmployeesUsingJPAQL() {
		return employeeRepository.getAllEmployeesUsingJPAQL();
	}
	
//	@Override
//	public List<Employee> getEmployeeByNameUsingJPQL(String firstName) {
//		return employeeRepository.getEmployeeByNameUsingJPQL(firstName);
//	}


	@Override
	public Employee updateEmployee(Employee employee, long id) {
		
		// pehle check employee id hai bhi ya nahi
		Employee existingEmployee = employeeRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Employee", "Id", id)); 
		
		existingEmployee.setFirstName(employee.getFirstName());
		existingEmployee.setLastName(employee.getLastName());
		existingEmployee.setEmail(employee.getEmail());
		// save existing employee to DB
		employeeRepository.save(existingEmployee);
		return existingEmployee;
	}

	@Override
	public void deleteEmployee(long id) {
		
		// pehle check
		employeeRepository.findById(id).orElseThrow(() -> 
								new ResourceNotFoundException("Employee", "Id", id));
		employeeRepository.deleteById(id);
	}
	
}
