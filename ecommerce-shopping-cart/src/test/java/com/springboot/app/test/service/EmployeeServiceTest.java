package com.springboot.app.test.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import com.springboot.app.model.Employee;
import com.springboot.app.service.EmployeeService;





@SpringBootTest
public class EmployeeServiceTest {

	@Mock
	private EmployeeService employeeService;
	@Test
	   public void whenUserIdIsProvided_thenRetrievedNameIsCorrect() {
		  Employee employee = new Employee();
		  employee.setFirstName("Niharika");
		  employee.setLastName("Joshi");
		  employee.setEmail("niharika.apeejay");
		  employee.setId(2);
	      Mockito.when(employeeService.getEmployeeById(2)).thenReturn(employee);
	      Employee
	      employee2 = employeeService.getEmployeeById(1);
	      assertTrue(employee2==null);
	      //assertTrue(employee2.getId()==1);
	   }

}
