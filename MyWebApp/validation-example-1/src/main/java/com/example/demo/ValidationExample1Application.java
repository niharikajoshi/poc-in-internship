package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidationExample1Application {

	public static void main(String[] args) {
		SpringApplication.run(ValidationExample1Application.class, args);
	}

}
