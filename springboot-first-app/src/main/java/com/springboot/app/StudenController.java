package com.springboot.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudenController {

@GetMapping("/student")
public Student getStudent(){
	return new Student("Niharika","Joshi");
}


//PathVariable
@GetMapping("/student/{firstName}/{lastName}")
public Student studentPathVariable(@PathVariable("firstName")String firstName,@PathVariable("lastName") String lastName) {
	return new Student(firstName,lastName);
}

//build rest API to handle query parameters
//https://localhost:8080/student?firstName=Niharika

 
}
